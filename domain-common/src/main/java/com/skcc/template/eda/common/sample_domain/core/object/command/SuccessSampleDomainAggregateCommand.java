package com.skcc.template.eda.common.sample_domain.core.object.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

@AllArgsConstructor
@ToString
@Getter
public class SuccessSampleDomainAggregateCommand {
  @TargetAggregateIdentifier
  private String aggregateId;
  private String id;

  @Builder
  public SuccessSampleDomainAggregateCommand(String id){
    this.aggregateId = UUID.randomUUID().toString();
    this.id = id;
  }
}