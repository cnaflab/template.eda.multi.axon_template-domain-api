package com.skcc.template.eda.common.sample_domain.core.object.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Getter
@Builder
public class SuccessSampleDomainAggregateEvent {
    private String id;
}
