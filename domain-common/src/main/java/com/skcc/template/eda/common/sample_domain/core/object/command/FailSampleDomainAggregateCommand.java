package com.skcc.template.eda.common.sample_domain.core.object.command;

import com.skcc.template.eda.common.sample_domain.core.object.event.FailSampleDomainAggregateEvent;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

@AllArgsConstructor
@ToString
@Getter
public class FailSampleDomainAggregateCommand {
  @TargetAggregateIdentifier
  private String aggregateId;
  private String id;

  @Builder
  public FailSampleDomainAggregateCommand(String id){
    this.id = id;
    this.aggregateId = UUID.randomUUID().toString();
  }
}