package com.skcc.template.eda.common.sample_domain.core.object.query;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
public class SampleDomainQueryMessage {
    private String id;
    private String domainId;
}