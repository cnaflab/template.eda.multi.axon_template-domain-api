package com.skcc.template.eda.sample_domain.core.application.service;

import java.util.List;

import com.skcc.template.eda.sample_domain.core.application.object.SampleDomainProjctionDTO;
import com.skcc.template.eda.sample_domain.core.application.object.SampleDomainQueryResponseDTO;
import com.skcc.template.eda.sample_domain.core.application.object.SampleDomainRequestDTO;
import com.skcc.template.eda.sample_domain.core.port_infra.SampleDomainProjectionRepository;
import com.skcc.template.eda.sample_domain.core.port_infra.SampleDomainQueryRepository;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class SampleDomainQueryService implements ISampleDomainQueryService {

    private final SampleDomainQueryRepository sampleDomainQueryRepository;
    private final SampleDomainProjectionRepository sampleDomainProjectionRepository;

    @Override
    public void putSampleDataAggregateStatus( SampleDomainProjctionDTO sampleDomainProjctionDTO ){
        log.debug("[Application Service Called] putSampleDataAggregateStatus");
        log.debug("SampleDomainProjctionDTO[{}]", sampleDomainProjctionDTO);

        sampleDomainProjectionRepository.save(sampleDomainProjctionDTO);
    }

    @Override
    public List<SampleDomainQueryResponseDTO> getDomainQueryData( SampleDomainRequestDTO sampleDomainRequestDTO ){
        log.debug("[Application Service Called] getDomainQueryData");
        log.debug("SampleDomainRequestDTO[{}]", sampleDomainRequestDTO);

        List<SampleDomainQueryResponseDTO> queryResult = sampleDomainQueryRepository.findAllByDomainId(sampleDomainRequestDTO.getDomainId());
        log.debug("Query Result[{}]", queryResult);

        return queryResult;
    }
}
