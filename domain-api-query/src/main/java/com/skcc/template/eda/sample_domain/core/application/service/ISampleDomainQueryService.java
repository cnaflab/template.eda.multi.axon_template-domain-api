package com.skcc.template.eda.sample_domain.core.application.service;

import java.util.List;

import com.skcc.template.eda.sample_domain.core.application.object.SampleDomainProjctionDTO;
import com.skcc.template.eda.sample_domain.core.application.object.SampleDomainQueryResponseDTO;
import com.skcc.template.eda.sample_domain.core.application.object.SampleDomainRequestDTO;

public interface ISampleDomainQueryService {

    public void putSampleDataAggregateStatus( SampleDomainProjctionDTO sampleDomainProjctionDTO );

    public List<SampleDomainQueryResponseDTO> getDomainQueryData( SampleDomainRequestDTO sampleDomainRequestDTO );
}
