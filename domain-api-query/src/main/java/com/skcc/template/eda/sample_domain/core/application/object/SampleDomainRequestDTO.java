package com.skcc.template.eda.sample_domain.core.application.object;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter @Setter
public class SampleDomainRequestDTO {
    private String id;
    private String domainId;
}