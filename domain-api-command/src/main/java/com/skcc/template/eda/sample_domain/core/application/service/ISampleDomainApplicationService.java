package com.skcc.template.eda.sample_domain.core.application.service;

import com.skcc.template.eda.common.sample_domain.core.object.command.CreateSampleDomainAggregateCommand;
import com.skcc.template.eda.common.sample_domain.core.object.event.CreateSampleDomainAggregateEvent;
import com.skcc.template.eda.common.sample_domain.core.object.event.FailSampleDomainAggregateEvent;
import com.skcc.template.eda.common.sample_domain.core.object.event.SuccessSampleDomainAggregateEvent;

public interface ISampleDomainApplicationService {
    
    public void createSampleDomainData(CreateSampleDomainAggregateEvent event) throws Exception;
    public void successSampleDomainData(SuccessSampleDomainAggregateEvent event) throws Exception;
    public void failSampleDomainData(FailSampleDomainAggregateEvent event) throws Exception;
}
