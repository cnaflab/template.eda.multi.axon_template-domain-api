package com.skcc.template.eda.sample_domain.infrastructure.persistent.axon;

import com.skcc.template.eda.common.exception.InvalidStatusForCommandException;
import com.skcc.template.eda.common.sample_domain.core.object.command.CreateSampleDomainAggregateCommand;
import com.skcc.template.eda.common.sample_domain.core.object.command.FailSampleDomainAggregateCommand;
import com.skcc.template.eda.common.sample_domain.core.object.command.PlaceSampleDomainAggregateCommand;
import com.skcc.template.eda.common.sample_domain.core.object.command.SuccessSampleDomainAggregateCommand;
import com.skcc.template.eda.common.sample_domain.core.object.event.CreateSampleDomainAggregateEvent;
import com.skcc.template.eda.common.sample_domain.core.object.event.FailSampleDomainAggregateEvent;
import com.skcc.template.eda.common.sample_domain.core.object.event.PlaceSampleDomainAggregateEvent;

import com.skcc.template.eda.common.sample_domain.core.object.event.SuccessSampleDomainAggregateEvent;
import com.skcc.template.eda.sample_domain.core.domain.entity.SampleDomainStatusVO;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * 클래스명 : SampleDomainAggregate
 * 내용 : Axon Server 연동 부분
 *  - AggregateIdentifier 값은 Unique 해야함
 *  - AggregateIdentifier 값이 Command 의  값과 같아야 함
 *  - Aggregate id 기준으로 command 를 수신하고, 그에 맞는 doEvent 를 발행한다.
 */

@Slf4j
@ToString
@Aggregate
public class SampleDomainAggregate {

    @AggregateIdentifier
    private String aggregateId;
    private String id;
    private SampleDomainStatusVO status;
    private String sampleData1;
    private String sampleData2;

    protected SampleDomainAggregate() {}

    @CommandHandler
    public SampleDomainAggregate(CreateSampleDomainAggregateCommand cmd) {
        log.debug("[Aggregate Called] SampleDomainAggregate[CreateSampleDomainAggregateCommand][" + this.toString() + "]");
        this.aggregateId = cmd.getAggregateId();
        this.id = cmd.getId();      //target id 와 맞추어 주어야 함
        this.sampleData1 = cmd.getSampleData1();
        this.sampleData2 = cmd.getSampleData2();
        this.status = SampleDomainStatusVO.SUBMIT;

        /**
         * Command 에 대한 로직을 처리하기 위한 Event 발행
         */
        log.debug("[Event dispatched] CreateSampleDomainAggregateEvent");
        AggregateLifecycle.apply(CreateSampleDomainAggregateEvent.builder()
                        .id(cmd.getId())
                        .sampleData1(cmd.getSampleData1())
                        .sampleData2(cmd.getSampleData2())
                        .status(SampleDomainStatusVO.SUBMIT.toString())
                        .build());
    }

    @EventSourcingHandler
    protected void eventSrouce(PlaceSampleDomainAggregateEvent event){
        this.id = event.getId();
        this.status = SampleDomainStatusVO.valueOf(event.getStatus());
        log.debug("[Aggregate Called][EventSourcingHandler] SampleDomainAggregateEvent[" + this.toString() + "]");
    }

    @CommandHandler
    public SampleDomainAggregate(PlaceSampleDomainAggregateCommand cmd) throws Exception {
        log.debug("[Aggregate Called][Command Handle] handle[PlaceSampleDomainAggregateCommand]");

        // Validate the last Aggregate Status by Parameter Command
        if(this.status != SampleDomainStatusVO.SUBMIT) throw new InvalidStatusForCommandException(cmd, this.status);
        this.aggregateId = cmd.getAggregateId();
        // if allowable status, status change and publish the event (Aggregate Status Changed)
        this.id = cmd.getId();
        this.status = SampleDomainStatusVO.PLACED;
        this.sampleData1 = cmd.getSampleData1();
        this.sampleData2 = cmd.getSampleData2();

        log.debug("[Event dispatched] PlaceSampleDomainAggregateEvent");
        AggregateLifecycle.apply(PlaceSampleDomainAggregateEvent.builder()
                    .sampleData1(cmd.getSampleData1())
                    .sampleData2(cmd.getSampleData2())
                    .build());
    }

    @CommandHandler
    public SampleDomainAggregate(SuccessSampleDomainAggregateCommand cmd) throws Exception {
        log.debug("[Aggregate Called][Command Handle] handle[SuccessSampleDomainAggregateCommand]");
        this.aggregateId = cmd.getAggregateId();
        // if allowable status, status change and publish the event (Aggregate Status Changed)
        this.id = cmd.getId();
        this.status = SampleDomainStatusVO.SUCCESS;

        log.debug("[Event dispatched] SuccessSampleDomainAggregateEvent");
        AggregateLifecycle.apply(SuccessSampleDomainAggregateEvent.builder().id(cmd.getId()));
    }

    @CommandHandler
    public SampleDomainAggregate(FailSampleDomainAggregateCommand cmd) throws Exception {
        log.debug("[Aggregate Called][Command Handle] handle[FailSampleDomainAggregateCommand]");
        this.aggregateId = cmd.getAggregateId();
        // Validate the last Aggregate Status by Parameter Command
        if(this.status != SampleDomainStatusVO.PLACED) throw new InvalidStatusForCommandException(cmd, this.status);

        // if allowable status, status change and publish the event (Aggregate Status Changed)
        this.id = cmd.getId();
        this.status = SampleDomainStatusVO.FAIL;

        log.debug("[Event dispatched] FailSampleDomainAggregateEvent");
        AggregateLifecycle.apply(FailSampleDomainAggregateEvent.builder().id(cmd.getId()).build());
	}
}