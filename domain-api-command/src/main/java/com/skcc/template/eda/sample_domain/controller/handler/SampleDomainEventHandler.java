package com.skcc.template.eda.sample_domain.controller.handler;

import com.skcc.template.eda.common.sample_domain.core.object.event.CreateSampleDomainAggregateEvent;
import com.skcc.template.eda.common.sample_domain.core.object.event.FailSampleDomainAggregateEvent;
import com.skcc.template.eda.common.sample_domain.core.object.event.SuccessSampleDomainAggregateEvent;
import com.skcc.template.eda.sample_domain.core.application.service.ISampleDomainApplicationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventhandling.Timestamp;
import org.springframework.stereotype.Component;

import java.time.Instant;

/**
 * 클래스명 : SampleDomainEventHandler
 * 내용 : 내부, 외부의 수신된 이벤트에 대한 로직 처리 (C,U,D 관련)
 *
 */

@AllArgsConstructor
@Slf4j
@Component
@ProcessingGroup("SampleDomainEventHandler")
public class SampleDomainEventHandler {

    private final ISampleDomainApplicationService iSampleDomainApplicationService;


    @EventHandler
    protected void on(CreateSampleDomainAggregateEvent event, @Timestamp Instant instant) throws Exception {
        log.debug("[Projection Handler Called] CreateSampleDomainAggregateEvent");
        log.debug("projecting {} , timestamp : {}", event, instant.toString());
        iSampleDomainApplicationService.createSampleDomainData(event);
    }

    @EventHandler
    protected  void on(SuccessSampleDomainAggregateEvent event, @Timestamp Instant instant) throws Exception{
        log.debug("[Projection Handler Called] SuccessSampleDomainAggregateEvent");
        log.debug("projecting {} , timestamp : {}", event, instant.toString());
        iSampleDomainApplicationService.successSampleDomainData(event);

    }

    @EventHandler
    protected  void on(FailSampleDomainAggregateEvent event, @Timestamp Instant instant) throws Exception{
        log.debug("[Projection Handler Called] FailSampleDomainAggregateEvent");
        log.debug("projecting {} , timestamp : {}", event, instant.toString());
        iSampleDomainApplicationService.failSampleDomainData(event);
    }
}
