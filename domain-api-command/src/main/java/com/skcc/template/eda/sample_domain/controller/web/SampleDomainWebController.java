package com.skcc.template.eda.sample_domain.controller.web;

import java.util.UUID;

import com.skcc.template.eda.common.sample_domain.core.object.command.CreateSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainRequestDTO;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/api/sample")
public class SampleDomainWebController {

    private final CommandGateway commandGateway;

    @PostMapping("/create")
    public ResponseEntity<Object> generateCreateSampleDomainAggregateCommand(@RequestBody SampleDomainRequestDTO sampleDomainRequestDTO){
        log.debug("[Web Controller Called] generateCreateSampleDomainAggregateCommand");

        /**
         * 1. Sample Data Generate
         */
        {
            String id = UUID.randomUUID().toString();
            sampleDomainRequestDTO = new SampleDomainRequestDTO(
                id
                , "SampleData1-" + id
                , "SampleData1-" + id
                , sampleDomainRequestDTO.getIsExternalError() );
        }

        /**
         * 2. Generate Aggregate Create Command
         */
        commandGateway.send(CreateSampleDomainAggregateCommand.builder()
                        .id(sampleDomainRequestDTO.getId())
                        .sampleData1(sampleDomainRequestDTO.getSampleData1())
                        .sampleData2(sampleDomainRequestDTO.getSampleData2())
                        .build());

        return new ResponseEntity<>(HttpStatus.OK);
    }

}